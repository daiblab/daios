var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var coinHistorysSchema = new Schema({
    coinId: String,
    type: String,                   // ether, btc
    price: Number,                   // 구입한 금액
    dai: Number,
    status: Boolean,                // false:거래완료 이전, true:거래완료
    regDate: String,                // instance가 만들어 졌을때 시간
    completeDate: String,            // 거래완료 되었을때 시간
    // from_userId: String,
    // to_userId: String,
});


module.exports = coinHistorysSchema;
