var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var coinsSchema = new Schema({
    userId: String,
    dai_address: String,               //bitweb block chain address
    btc_address: String,                //user block chain address
    ether_address: String,
    total_dai: Number,
    // total_btc: Number,
    // total_ether: Number,
    // historys: [String],
    ether_historys: [String],           //ether를 입금 했을때
    btc_historys: [String],             //btc를 입금 했을때
    dai_historys: [String],            //dai를 입금 했을때
    ether_withdraw_historys: [String],  //ether를 출금 했을때
    btc_withdraw_historys: [String],    //btc를 출금 했을때
    dai_withdraw_historys: [String],   //dai를 출금 했을때

});

module.exports = coinsSchema;
