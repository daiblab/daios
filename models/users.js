var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var usersSchema = new Schema({
    email: String,
    // etherId: ObjectId,
    first_name: String,
    last_name: String,
    // phone_number: String,
    country: String,
    googleId: String,
    provider: String,
    sessionId: String,

    userTag: String,
    // userName: String,
    // password: String,
    active: Boolean,
    phone: String,
    country: String, //한국:kr, 중국:cn
    // mail_auth_code: String,
    regDate: String,          //회원가입 날짜
    // updateDate: String,
    // lastLoginData: String,
    etherId: ObjectId,
    // bankId: ObjectId,
    agreementId: ObjectId,
    coinId: ObjectId,
    // pointId: ObjectId,
});

module.exports = usersSchema;
