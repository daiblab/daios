var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var ethersSchema = new Schema({
    address: String,                // 유저의 지갑 주소
    token_address_contract: String, // daios의 지갑 주소
    total_coin: Number,             // 유저가 전송할 총 ETH,
    bonus: Number,                   // 보너스
    total_daios: Number,            // 유저가 구매할 총 DAOIS
    historys: [String]              // etherHistoryId List
});

module.exports = ethersSchema;
