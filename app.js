var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');

var oauthGoogle = require('./routes/passport/google/google');
var oauthGoogleCallback = require('./routes/passport/google/googleCallback');
var indexRouter = require('./routes/router');
var usersRouter = require('./routes/users');
var ethersRouter = require('./routes/ethers');
var etherHistorysRouter = require('./routes/etherHistorys');
var notificationsRouter = require('./routes/notifications');
var coinsRouter = require('./routes/coins');
var coinHistorysRouter = require('./routes/coinHistorys');

var app = express();

let config = require('./config/config');
let MONGODB_URL = config.db.mongodb_url;
let DB_FILE = config.db.db_name;
let DB_URI = 'mongodb://' + MONGODB_URL + '/' + DB_FILE;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/blockchain', express.static(__dirname + '/blockchain')); // redirect JS jQuery

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

passport.serializeUser(function(profile, done) {
    console.log('serializeUser =>' + JSON.stringify(profile));

    let data = {
        "googleId": profile.id,
        "email": profile.email,
        "provider": profile.provider,
        "sessionId": profile.sessionID
    }

    if (data.email != null
        || data.email != undefined) {
        let db = require('./utils/db')
        let daiosUsers = require('./services/users')
        var controllerUsers = require('./controllers/users')
        var controllerCoins = require('./controllers/coins');
        var controllerAgreements = require('./controllers/agreements');

        db.connectDB()
            .then(() => daiosUsers.getUserByEmail(data.email))
            .then((user) => {
                console.log('db user=>', user)

                if (user == null) {
                    data['userTag'] = data.email;
                    daiosUsers.createUser(data)
                        .then((result) => {

                            let userId = result._id;
                            profile['userTag'] = result.userTag;
                            profile['userId'] = userId;
                            profile['newYn'] = 'Y';

                            controllerCoins.createByCoin()
                                .then(result => {
                                    if (result._id != null) {
                                        let coinId = result._id;
                                        console.log('coinId=>', coinId)
                                        controllerAgreements.createByAgreement()
                                            .then(result => {
                                                if (result._id != null) {
                                                    let body = {}
                                                    let agreementId = result._id;
                                                    controllerUsers.updateWithIds(body, userId, coinId, agreementId)
                                                        .then(result => {
                                                            console.log('result=>', result)
                                                            done(null, profile);
                                                        })
                                                }
                                            }).catch((err) => {
                                            console.error('err=>', err)
                                        })
                                    }
                                })
                                .catch((err) => {
                                    console.error('err=>', err)
                                })
                        })
                } else {
                    profile['userId'] = user._id;
                    profile['newYn'] = 'N';
                    console.log('exist user =>', profile);
                    done(null, profile);
                }
            }).catch((err) => {
                console.log('reject err=>', err);
        })
    }


});

passport.deserializeUser(function(obj, done) {
    console.log('deserializeUser =>' + JSON.stringify(obj));
    done(null, obj);
});

app.use(passport.initialize());
// app.use(passport.session());

app.use(session({
    secret: 'daios123', //Only enable https
    name: 'daios',
    store: new MongoStore({ url: DB_URI}), // connect-mongo session store
    proxy: false,
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: 60 * 60 * 24 * 30 * 10000 // 쿠키 유효기간 하루 (24시간) * 30일 //현재 무기한
    }
}));




app.use('/', indexRouter);

//backend API
let version = "/v1";
app.use('/auth/google', oauthGoogle);
app.use('/auth/google/callback', oauthGoogleCallback);
app.use(version + '/users', usersRouter);
app.use(version + '/coins', coinsRouter);
app.use(version + '/coins/historys', coinHistorysRouter);
app.use(version + '/notifications/email', notificationsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  //next(createError(404));
    res.render('404');
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
    res.render('404');
});

module.exports = app;
