var express = require('express');
var router = express.Router();
var controllerEthers = require('../controllers/ethers')
var DaiosResponse = require('../utils/DaiosResponse')
var mqtt = require('../utils/mqtt');

router.get('/:etherId', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.params.etherId != null) {
        controllerEthers.get(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }

});

router.get('/:etherId/price', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.params.etherId != null) {
        controllerEthers.getPrice(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            res.status(500).send(daiosResponse.create())
        })
    }

});

router.post('/', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.body.etherId != null) {
        controllerEthers.create(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
})

router.put('/:etherId', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.params.etherId != null) {

        if (req.body.coin != null || req.body.coin != undefined) {

            let controllerEtherHistorys = require('../controllers/etherHistorys');
            let etherId = req.params.etherId;
            let body = req.body;

            controllerEtherHistorys.createEtherHistoryByEtherId(etherId, body)
                .then(result => {
                    let historyId = result._id;

                    //TODO: test total_coin => historyCoin
                    controllerEthers.updateWithHistory(etherId, historyId)
                        .then(result => {
                            controllerEthers.getEther(etherId)
                                .then(result => {

                                    console.log('getEther result=>', result);
                                    //TODO: test json data
                                    let historyCoin = req.body.coin;
                                    let historyCount = result.historys.length;
                                    var jsonData = {};
                                    jsonData['etherId'] = etherId;
                                    jsonData['historyId'] = historyId;
                                    jsonData['coin'] = historyCoin;
                                    jsonData['address'] = result.address;
                                    jsonData['token_address_contract'] = result.token_address_contract;
                                    jsonData['address_count'] = historyCount;
                                    jsonData['total_coin'] = result.total_coin;
                                    jsonData['total_daios'] = result.total_daios;
                                    jsonData['historyCount'] = historyCount;


                                    mqtt.publishEtherScanSchduler(jsonData)
                                    mqtt.subscribeEtherScanSchduler();

                                    daiosResponse.code = 200;
                                    daiosResponse.data = result;
                                    res.status(200).send(daiosResponse.create())
                                });
                        })
                }).catch((err) => {
                console.error('err=>', err)
                daiosResponse.code = 500;
                daiosResponse.message = err;
                res.status(500).send(daiosResponse.create())
            });
        } else {
            controllerEthers.update(req)
                .then(result => {
                    daiosResponse.code = 200;
                    daiosResponse.data = result;
                    res.status(200).send(daiosResponse.create())
                }).catch((err) => {
                console.error('err=>', err)
                daiosResponse.code = 500;
                daiosResponse.message = err;
                res.status(500).send(daiosResponse.create())
            })
        }
    }
});

module.exports = router;
