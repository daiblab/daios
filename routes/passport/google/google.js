'use strict';

var express = require('express');
var router = express.Router();
var passport = require('passport');
var passportStrategy = require('./../passportStrategy');

passport.use(passportStrategy.googleStrategy);
router.get('/',
    passport.authenticate('google',
        {scope: [
                // 'https://www.googleapis.com/auth/plus.login',
                // 'https://www.googleapis.com/auth/contacts.readonly',
                'https://www.googleapis.com/auth/plus.profile.emails.read'
            ]}),
    function (req, res) {
        console.log(res.body);
    });
module.exports = router;
