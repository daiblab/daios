'use strict';

var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../../../config/config');

router.get('/',
    passport.authenticate('google',
        {
            // successRedirect: '/signup',
            failureRedirect: '/' }),
    function(req, res) {
        var userId = req.session.passport.user.userId;
        var userTag = req.session.passport.user.userTag;

        if (userTag != undefined) req.session.userTag = userTag;
        console.log('req session passport userTag=>', userTag);

        var redirectUrl = '/signup';

        if (req.session.passport.user.newYn === 'N') {
            redirectUrl = '/transaction';
        } else {
            redirectUrl += '?userId=' + userId;
        }
        // var redirectUrl = '/';
        res.redirect(redirectUrl);
    });


module.exports = router;
