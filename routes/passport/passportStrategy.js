'use strict';

var GoogleStrategy = require('passport-google-oauth2').Strategy;
var config = require('../../config/config');

exports.googleStrategy = new GoogleStrategy({
        clientID: config.google.clientID,
        clientSecret: config.google.clientSecret,
        callbackURL: config.google.callbackURL,
        passReqToCallback   : true
    },
    function(request, accessToken, refreshToken, profile, done) {
        console.log('token = ' + accessToken + ' / refreshToken = ' + refreshToken + ' / profile = ' + profile );
        config.google.accessToken = accessToken;

        console.log('request.sessionID=>', request.sessionID)
        profile['sessionID'] = request.sessionID;

        process.nextTick(function () {
            return done(null, profile);
        });
    }
)
