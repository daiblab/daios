var express = require('express');
var router = express.Router();
var controllerCoinHistorys = require('../controllers/coinHistorys')
var DaiosResponse = require('../utils/DaiosResponse')

router.get('/:historyId/deposit/coinTypes/:coinType', function (req, res, next) {

    console.log('test')
    var daiosResponse = new DaiosResponse();
    if (req.params.historyId != null) {
        controllerCoinHistorys.getDeposit(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }

});

router.get('/:historyId/withdraw/coinTypes/:coinType', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.params.historyId != null) {
        controllerCoinHistorys.getWithdraw(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }

});

module.exports = router;
