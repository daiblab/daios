var express = require('express');
var router = express.Router();
var DaiosResponse = require('../utils/DaiosResponse')
var mqtt = require('../utils/mqtt');

router.post('/', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.body.from != null) {

        let jsonData = {};
        // jsonData['to'] = 'sales@daiblab.com'
        jsonData['to'] = 'mailer@daiblab.com';
        jsonData['from'] = req.body.from;
        jsonData['subject'] = req.body.subject;
        jsonData['text'] = req.body.text;

        mqtt.publishEmail(jsonData)
            .then((result) => {
                // mqtt.subscribeEmail();
                daiosResponse.code = 200;
                daiosResponse.data = "Your e-mail registration is complete!";
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    } else {

        var err = "Need email in req body"
        console.error('err=>', err)
        daiosResponse.code = 500;
        daiosResponse.message = err;
        res.status(400).send(daiosResponse.create())
    }
})


module.exports = router;
