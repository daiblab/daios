var express = require('express');
var router = express.Router();
let db = require('../utils/db')
var daiosUsers = require('../services/users');
var daiosEthers = require('../services/ethers');
var datetime = require('node-datetime');
var dbconfig=require("../config/config");

/* GET home page. */
router.get('/', function(req, res, next) {
    let loginStatus = false;
    let presalesYn = req.session.preSalesYN || "N".toString();
    if(req.session.passport != undefined) {
        loginStatus = true;
    }
    res.render('index_en', { title: 'DAIOS Rending Page', loginStatus: loginStatus, presalesYn:presalesYn });
});

/* GET home page. */
router.get('/ko', function(req, res, next) {
    let loginStatus = false;
    let presalesYn = req.session.preSalesYN;
    if(req.session.passport != undefined) {
        loginStatus = true;
    }
    res.render('index', { title: 'DAIOS Rending Page', loginStatus: loginStatus, presalesYn:presalesYn });
});

router.get('/signup', eventDay, function(req, res, next) {
    let userinfo = {
        sessionID: req.sessionID,
        userId: req.session.passport.user.userId
    };

    db.connectDB()
        .then(() => daiosUsers.getUserById(userinfo.userId))
        .then((user) => {
            console.log('result user =>', user);
            userinfo.user = user._doc;
            res.render('register', { title: 'DAIOS Sign Up', data: userinfo });
        }).catch((err) => {
            console.log('reject err=>', err);
            res.render('register', { title: 'DAIOS Sign Up', data: error });
    })
});

router.get('/myPage', eventDay, function(req, res, next) {
    let userinfo = {
        userId: req.session.passport.user.userId,
        sessionID: req.sessionID,
    };

    db.connectDB()
        .then(() => daiosUsers.getUserById(userinfo.userId))
        .then((user) => {
            console.log('result user =>', user);
            userinfo.user = user._doc;
            if(userinfo.user.etherId != undefined) {
                userinfo.etherId = userinfo.user.etherId;

                daiosEthers.getEtherById(userinfo.etherId).then((ethers) => {
                    userinfo.ethers = ethers._doc;
                    console.log(ethers._doc);
                    res.render('myPage', {title: 'DAIOS My Transaction', data: userinfo});
                });
            } else {
                res.render('myPage', {title: 'DAIOS My Transaction', data: userinfo});
            }
        }).catch((err) => {
        console.log('reject err=>', err);
        res.render('myPage', {title: 'DAIOS My Transaction', data: userinfo});
    })


});

router.get('/transaction', eventDay, function(req, res, next) {
    let userinfo = {
        userId: req.session.passport.user.userId,
        sessionID: req.sessionID,
    };

    db.connectDB()
        .then(() => daiosUsers.getUserById(userinfo.userId))
        .then((user) => {
            console.log('result user =>', user);
            console.log('result userinfo =>', userinfo);
            userinfo.user = user._doc;
            if(userinfo.user.etherId != undefined) {
                userinfo.etherId = userinfo.user.etherId;

                daiosEthers.getEtherById(userinfo.etherId).then((ethers) => {
                    userinfo.ethers = ethers._doc;
                    console.log(ethers._doc);
                    res.render('myTransaction', {title: 'DAIOS My Transaction', data: userinfo, btcAddress: dbconfig.testnet.address.btc,
                        etherAddress: dbconfig.mainnet.address.ether});
                });
            } else {
                res.render('myTransaction', {title: 'DAIOS My Transaction', data: userinfo, btcAddress: dbconfig.testnet.address.btc,
                    etherAddress: dbconfig.mainnet.address.ether});
            }
        }).catch((err) => {
            console.log('reject err=>', err);
            res.render('myTransaction', {title: 'DAIOS My Transaction', data: userinfo, btcAddress: dbconfig.testnet.address.btc,
                etherAddress: dbconfig.mainnet.address.ether});
    })
});

router.get('/howtouse', function(req, res, next) {
    res.render('howtouse', { title: 'DAIOS Rending Page' });
});

router.get('/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/');
});

router.get('/notsales', function(req, res, next) {
    res.render('notsales', { title: 'DAIOS Rending Page' });
});
/* GET users listing. */
// router.get('/users', function(req, res, next) {
//     res.send('respond with a resource');
// });

function eventDay(req, res, next) {

    let currentTime = createCurrentUnixTime();
    let firstStartDay = '2019-01-01 00:00:00'
    let firstEndDay = '2019-02-04 23:59:59'

    console.log('currentTime        =>', currentTime)
    console.log('firstStartDay      =>', createUnixTime(firstStartDay))
    console.log('firstEndDay        =>', createUnixTime(firstEndDay))

    switch (true) {
        case ((createUnixTime(firstStartDay) <= currentTime) &&
            (createUnixTime(firstEndDay) >= currentTime)):
            console.log('event term!')
            req.session.preSalesYN =  'Y';
            next();
            break;
        default:
            req.session.preSalesYN = 'N';
            res.redirect('/notsales');
            break;
    }

}

var createCurrentUnixTime = function () {
    let koTime = new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '');
    let pastDateTime = datetime.create(koTime);
    // console.log('koTime=>', koTime)
    // console.log('pastDateTime getTime=>', pastDateTime.getTime())

    return pastDateTime.getTime();
}

var createUnixTime = function (day) {
    let pastDateTime = datetime.create(day);
    return pastDateTime.getTime();
}

module.exports = router;
