var express = require('express');
var router = express.Router();
var controllerCoins = require('../controllers/coins')
var controllerCoinHistorys = require('../controllers/coinHistorys');
var DaiosResponse = require('../utils/DaiosResponse')
var mqtt = require('../utils/mqtt');
var unixtime = require('unix-timestamp')

router.get('/:coinId', function (req, res, next) {
    // res.send('respond with a resource');

    var daiosResponse = new DaiosResponse();
    if (req.params.coinId != null) {
        controllerCoins.get(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }

});

router.get('/:coinId/deposit/historys', function (req, res, next) {

    var daiosResponse = new DaiosResponse();

    if (req.params.coinId != null) {

        let pageIdx = req.query.pageIdx;
        let perPage = req.query.perPage;

        if (pageIdx != undefined) pageIdx = parseInt(pageIdx);
        if (perPage != undefined) perPage = parseInt(perPage);

        let initPageIdx = pageIdx;
        let initPerPage = perPage;
        let historys = []

        controllerCoins.get(req)
            .then(result => {

                if (result.ether_historys.length <= 0 && result.dai_historys.length <=0 && result.btc_historys.length <= 0) {

                    var err = "historys is empty !"
                    console.error('err=>', err)
                    daiosResponse.code = 403;
                    daiosResponse.message = err;
                    res.status(403).send(daiosResponse.create())
                } else {

                    let ether_historys = [];
                    let dai_historys = [];
                    let btc_historys = [];

                    if (result.ether_historys != undefined) ether_historys = result.ether_historys;
                    if (result.dai_historys != undefined) dai_historys = result.dai_historys;
                    if (result.btc_historys != undefined) btc_historys = result.btc_historys;

                    controllerCoinHistorys.getDepositHistorys(ether_historys, "ether")
                        .then(result => {
                            // result['_doc']['type'] = "ether"

                            if (result != undefined) historys.push.apply(historys, result)

                            controllerCoinHistorys.getDepositHistorys(dai_historys, "dai")
                                .then(result => {
                                    // result['_doc']['type'] = "dai"
                                    // console.log('ether=>', result)
                                    if (result != undefined) historys.push.apply(historys, result)

                                    controllerCoinHistorys.getDepositHistorys(btc_historys, "btc")
                                        .then(result => {
                                            // result['_doc']['type'] = "btc"
                                            // console.log('ether=>', result)
                                            if (result != undefined) historys.push.apply(historys, result)

                                            // compareDate(historys)
                                            historys.sort((first, second) => {
                                                return parseFloat(unixtime.fromDate(second.regDate)) - parseFloat(unixtime.fromDate(first.regDate))
                                            })


                                            if (pageIdx == undefined) {
                                                pageIdx = 0
                                                initPageIdx = pageIdx
                                            }
                                            if (perPage == undefined) {
                                                perPage = 5
                                                initPerPage = perPage
                                            }

                                            if (pageIdx != 0) pageIdx = pageIdx * perPage
                                            perPage = pageIdx + perPage

                                            historys = historys.slice(pageIdx, perPage)

                                            let jsonResult = {}
                                            jsonResult['historys'] = historys;
                                            jsonResult['pageIdx'] = initPageIdx;
                                            jsonResult['perPage'] = initPerPage;

                                            console.log('jsonResult=>', jsonResult)
                                            daiosResponse.code = 200;
                                            daiosResponse.data = jsonResult;
                                            res.status(200).send(daiosResponse.create())

                                        })
                                })
                        }).catch((err) => {
                        console.error('err=>', err)
                        daiosResponse.code = 500;
                        daiosResponse.message = err;
                        res.status(500).send(daiosResponse.create())
                    })
                }
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }

});

router.get('/:coinId/withdraw/historys', function (req, res, next) {

    var daiosResponse = new DaiosResponse();

    if (req.params.coinId != null) {

        let pageIdx = req.query.pageIdx;
        let perPage = req.query.perPage;

        if (pageIdx != undefined) pageIdx = parseInt(pageIdx);
        if (perPage != undefined) perPage = parseInt(perPage);

        let initPageIdx = pageIdx;
        let initPerPage = perPage;
        let historys = []

        controllerCoins.get(req)
            .then(result => {

                if (result.ether_withdraw_historys.length <= 0 && result.dai_withdraw_historys.length <=0 && result.btc_withdraw_historys.length <= 0) {

                    var err = "historys is empty !"
                    console.error('err=>', err)
                    daiosResponse.code = 403;
                    daiosResponse.message = err;
                    res.status(403).send(daiosResponse.create())
                } else {
                    let ether_historys = []
                    let dai_historys = []
                    let btc_historys = []

                    if (result.ether_withdraw_historys != undefined) ether_historys = result.ether_withdraw_historys;
                    if (result.btc_withdraw_historys != undefined) btc_historys = result.btc_withdraw_historys;
                    if (result.dai_withdraw_historys != undefined) dai_historys = result.dai_withdraw_historys;

                    controllerCoinHistorys.getWithdrawHistorys(ether_historys, "ether")
                        .then(result => {
                            // result['_doc']['type'] = "ether"
                            // console.log('ether=>', result)
                            if (result != undefined) historys.push.apply(historys, result)

                            controllerCoinHistorys.getWithdrawHistorys(dai_historys, "dai")
                                .then(result => {
                                    // result['_doc']['type'] = "dai"
                                    // console.log('ether=>', result)
                                    if (result != undefined) historys.push.apply(historys, result)

                                    controllerCoinHistorys.getWithdrawHistorys(btc_historys, "btc")
                                        .then(result => {
                                            // result['_doc']['type'] = "btc"
                                            // console.log('ether=>', result)
                                            if (result != undefined) historys.push.apply(historys, result)

                                            // compareDate(historys)
                                            historys.sort((first, second) => {
                                                return parseFloat(unixtime.fromDate(second.regDate)) - parseFloat(unixtime.fromDate(first.regDate))
                                            })

                                            if (pageIdx == undefined) {
                                                pageIdx = 0
                                                initPageIdx = pageIdx
                                            }
                                            if (perPage == undefined) {
                                                perPage = 5
                                                initPerPage = perPage
                                            }

                                            if (pageIdx != 0) pageIdx = pageIdx * perPage
                                            perPage = pageIdx + perPage

                                            historys = historys.slice(pageIdx, perPage)

                                            let jsonResult = {}
                                            jsonResult['historys'] = historys;
                                            jsonResult['pageIdx'] = initPageIdx;
                                            jsonResult['perPage'] = initPerPage;

                                            console.log('jsonResult=>', jsonResult)
                                            daiosResponse.code = 200;
                                            daiosResponse.data = jsonResult;
                                            res.status(200).send(daiosResponse.create())

                                        })
                                })
                        }).catch((err) => {
                        console.error('err=>', err)
                        daiosResponse.code = 500;
                        daiosResponse.message = err;
                        res.status(500).send(daiosResponse.create())
                    })
                }
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
});

function compareDate(historys, left, right) {

    let pivot = unixtime.fromDate(historys[left].regDate);
    let low = left + 1;
    let high = right;

    while (low <= high) {
        while (pivot >= unixtime.fromDate(historys[low].regDate) && low <= right) low ++
        while (pivot <= unixtime.fromDate(historys[high].regDate) && high >= left+1) high --

        if (low <= high) Swap(historys, low, high)

    }
    Swap(historys, left, high)


    return high;
}

function Sort(historys, left, right) {
    if (left <= right) {
        let pivot = compareDate(historys, left, right);
        Sort(historys, left, pivot - 1)
        Sort(historys, pivot + 1, right)
    }
}

function Swap(historys, first, second) {
    let tmp = historys[first]
    historys[first] = historys[second];
    historys[second] = tmp;
}

router.post('/', function (req, res, next) {

    let sampleJson =
        {
            "userId": "123",
            "dai_address": "asdf",
            "btc_address": "qewr",
            "ether_address": "zxcv",
            "total_dai": 0,
        }

    if (req.body.userId != null) {

        let data = {}

        var daiosResponse = new DaiosResponse();
        controllerCoins.create(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })

    }
});
router.put('/:coinId', function (req, res, next) {

    // "coinId": "5bc9a10b8f8b950ffe068df9"
    let sampleJson =
        {
            "userId": "5bc9a10de399867b00e2e30f",
            "dai_address": "asdf",
            "btc_address": "qewr",
            "ether_address": "zxcv"
        }

    let controllerCoins = require('../controllers/coins');

    var daiosResponse = new DaiosResponse();

    if (req.params.coinId != null) {

        let coinId = req.params.coinId;
        let body = req.body;

        controllerCoins.updateById(coinId, body)
            .then((result) => {

                let data = {}
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
})

router.put('/:coinId/deposit/coinTypes/:coinType', function (req, res, next) {

    /**
     * coinType: btc, ether, dai
     * price : dai를 btc, ether, dai로 환산된 금액 (유저가 지불할 금액)
     * dai : 입금 요청할 dai
     * */
    let sampleJson =
        {
            "price": 10,
            "dai": 20,
            "address": "test"
        }

    var daiosResponse = new DaiosResponse();
    if (req.params.coinId != null && req.params.coinType != null) {

        if (req.body.price != null) {

            let coinId = req.params.coinId;
            let coinType = req.params.coinType;
            let dai = req.body.dai;
            let price = req.body.price;
            let address = req.body.address

            controllerCoinHistorys.createCoinHistoryByCoinId(coinId, req)
                .then(result => {
                    let historyId = result._id;

                    //TODO: test total_coin => historyCoin
                    controllerCoins.updateWithHistory(coinType, coinId, historyId)
                        .then((result) => {
                            controllerCoins.getByCoinId(coinId)
                                .then(result => {
                                    console.log('getCoin result=>', result);
                                    let jsonData = {};
                                    let historyPrice = price;
                                    let historyCount = 0;

                                    if (coinType == "ether") {
                                        console.log('result.ether_historys.length=>', result._doc.ether_historys.length)
                                        if (result.ether_historys.length > 0) {
                                            historyCount = result.ether_historys.length;
                                            // for (var i = 0; i < result.btc_historys.length; i++) {
                                            //     historyCount++;
                                            // }
                                        }

                                    } else if (coinType == "btc") {
                                        console.log('result.btc_historys.length=>', result._doc.btc_historys.length)
                                        if (result.btc_historys.length > 0) {
                                            historyCount = result.btc_historys.length
                                            // for (var i = 0; i < result.btc_historys.length; i++) {
                                            //     historyCount++;
                                            // }
                                        }
                                    } else if (coinType == "dai") {
                                        console.log('result.dai_historys.length=>', result.dai_historys.length)
                                        if (result.dai_historys.length > 0) {

                                            historyCount = result.dai_historys.length
                                            // for (var i = 0; i < result.dai_historys.length; i++) {
                                            //     historyCount++;
                                            // }
                                        }
                                    }

                                    jsonData['coinId'] = coinId;
                                    jsonData['historyId'] = historyId;
                                    jsonData['type'] = coinType;
                                    jsonData['price'] = historyPrice;
                                    jsonData['history_count'] = historyCount;
                                    jsonData['address'] = address
                                    jsonData['ether_address'] = result.ether_address;
                                    jsonData['dai_address'] = result.dai_address;
                                    // jsonData['btc_address'] = result.btc_address;
                                    jsonData['btc_address'] = address;
                                    jsonData['dai'] = dai;
                                    jsonData['total_dai'] = result.total_dai;
                                    jsonData[''] = "dev";

                                    daiosResponse.code = 200;
                                    daiosResponse.data = result;
                                    res.status(200).send(daiosResponse.create())

                                    if (jsonData['type'] == "btc") {
                                        mqtt.btcJob(jsonData)
                                    } else {
                                        mqtt.DepositJob(jsonData)
                                    }

                                });
                        });
                }).catch((err) => {
                console.error('err=>', err)
                daiosResponse.code = 500;
                daiosResponse.message = err;
                res.status(500).send(daiosResponse.create())
            });
        }
    }
});

router.put('/:coinId/withdraw/coinTypes/:coinType', function (req, res, next) {

    /**
     * coinType: btc, ether, dai
     * price : dai를 btc, ether, dai로 환산된 금액 (유저가 받아갈 금액)
     * dai : 출금 요청할 dai
     * address : user address
     * */
    let sampleJson =
        {
            "price": 10,
            "dai": 20,
            "address": "0xdC76866e2457a7fafB7E55BbB84b14C4DdCCA42c"
        }

    var daiosResponse = new DaiosResponse();
    if ((req.params.coinId != null) && (req.params.coinType != null)) {

        let coinId = req.params.coinId;
        let coinType = req.params.coinType;
        let address = req.body.address;
        let dai = req.body.dai;
        let price = req.body.price;

        if (coinType == "ether" || coinType == "dai") {

            let ethereumWeb3 = require('../utils/ethereumWeb3');

            let data = {}
            data['type'] = coinType;
            data['price'] = price;
            data['address'] = address;

            ethereumWeb3.withDraw(data)
                .then(transaction => {
                    console.log('withDraw() =>', transaction);
                    let data = {}
                    data['coinType'] = coinType;
                    data['dai'] = dai;
                    data['price'] = price;
                    data['address'] = address;
                    data['transaction'] = transaction;

                    controllerCoinHistorys.createCoinWithdrawHistoryByCoinId(coinId, data)
                        .then(result => {

                            let historyId = result._id;
                            console.log('historyId->', historyId);

                            controllerCoins.updateWithWithdrawHistory(coinType, coinId, historyId)
                                .then((result) => {
                                    controllerCoins.getByCoinId(coinId)
                                        .then(result => {
                                            console.log('getCoin result=>', result);

                                            let jsonData = {}
                                            jsonData['coinId'] = coinId;
                                            jsonData['historyId'] = historyId;
                                            jsonData['type'] = coinType + "_withdraw";
                                            jsonData['price'] = price;
                                            jsonData['address'] = address;
                                            jsonData['transaction'] = transaction;
                                            jsonData['dai'] = dai;
                                            jsonData['total_dai'] = result.total_dai;

                                            mqtt.WithdrawJob(jsonData)

                                            daiosResponse.code = 200;
                                            daiosResponse.data = result;
                                            res.status(200).send(daiosResponse.create())

                                        }).catch(err => {
                                        console.log('err=>', err);
                                    })

                                }).catch((err => {
                                console.error('err=>', err)
                            }))
                        })
                })
        } else if (coinType == "btc") {
            let bitcore_lib = require('../utils/bitcore_lib');
            bitcore_lib.sendBtcTransaction(req.body)
                .then(transaction => {
                    console.log('sendBtcTransaction() =>', transaction);
                    let data = {}
                    data['coinType'] = coinType;
                    data['dai'] = dai;
                    data['price'] = price;
                    data['address'] = address;
                    data['transaction'] = transaction;

                    controllerCoinHistorys.createCoinWithdrawHistoryByCoinId(coinId, data)
                        .then(result => {

                            let historyId = result._id;
                            console.log('historyId->', historyId);

                            controllerCoins.updateWithWithdrawHistory(coinType, coinId, historyId)
                                .then((result) => {
                                    controllerCoins.getByCoinId(coinId)
                                        .then(result => {
                                            console.log('getCoin result=>', result);

                                            let jsonData = {}
                                            jsonData['coinId'] = coinId;
                                            jsonData['historyId'] = historyId;
                                            jsonData['type'] = coinType + "_withdraw";
                                            jsonData['price'] = price;
                                            jsonData['address'] = address;
                                            jsonData['transaction'] = transaction;
                                            jsonData['dai'] = dai;
                                            jsonData['total_dai'] = result.total_dai;

                                            mqtt.WithdrawBtcJob(jsonData)

                                            daiosResponse.code = 200;
                                            daiosResponse.data = result;
                                            res.status(200).send(daiosResponse.create())

                                        }).catch(err => {
                                        console.log('err=>', err);
                                    })

                                }).catch((err => {
                                console.error('err=>', err)
                            }))
                        })
                })

        } else {
            console.log("coinType undefined!")
        }
    }
});

router.delete('/:coinId', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.params.coinId != null) {
        controllerCoins.remove(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
})

module.exports = router;
