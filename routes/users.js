var express = require('express');
var router = express.Router();
var controllerUsers = require('../controllers/users')
var controllerCoins = require('../controllers/coins');
var controllerAgreements = require('../controllers/agreements');
var DaiosResponse = require('../utils/DaiosResponse')
var mqtt = require('../utils/mqtt');
const bitcore = require('bitcore-lib');
const Address = require('bitcore-explorers/node_modules/bitcore-lib/lib/address');

router.get('/:userId', function (req, res, next) {
    // res.send('respond with a resource');

    var daiosResponse = new DaiosResponse();
    if (req.params.userId != null) {
        controllerUsers.get(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    } else {
        daiosResponse.code = 400;
        daiosResponse.message = "Check the parameters.";
        res.status(400).send(daiosResponse.create())
    }

});

router.get('/userTag/:userTag', function (req, res, next) {

    var daiosResponse = new DaiosResponse();

    if (req.params.userTag != null) {

        let country = req.session.country;
        let userTag = req.params.userTag;
        controllerUsers.getByUserTag(userTag)
            .then(result => {

                if (result != null) {
                    let error = "해당 id는 이미 사용중입니다."
                    daiosResponse.code = 403;
                    daiosResponse.message = error;
                    res.status(403).send(daiosResponse.create())
                } else {
                    daiosResponse.code = 200;
                    daiosResponse.data = "ok";
                    res.status(200).send(daiosResponse.create())
                }

            }).catch((err) => {
            // console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
});

router.get('/email/:email', function (req, res, next) {
    // res.send('respond with a resource');

    var daiosResponse = new DaiosResponse();
    if (req.params.email != null) {

        let country = req.session.country;
        let email = req.params.email;
        controllerUsers.getByUserEmail(email)
            .then(result => {
                if (result != null) {
                    let error = "해당 email은 이미 사용중입니다."
                    daiosResponse.code = 403;
                    daiosResponse.message = error;
                    res.status(403).send(daiosResponse.create())
                } else {
                    daiosResponse.code = 200;
                    daiosResponse.data = "ok";
                    res.status(200).send(daiosResponse.create())
                }
            }).catch((err) => {
            // console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
});

router.get('/:userId/session/:sessionId', function (req, res, next) {
    // res.send('respond with a resource');

    var daiosResponse = new DaiosResponse();
    if (req.params.userId != null
        && req.params.sessionId != null) {

        controllerUsers.getUserIdAndSessionId(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    } else {
        daiosResponse.code = 400;
        daiosResponse.message = "Check the parameters.";
        res.status(400).send(daiosResponse.create())
    }

});

router.post('/', function (req, res, next) {

    let sampleJson =
        {
            "userTag": "kay",
            "password": "123",
            "email": "test@diablab.com",
            "phone": "010-123-4567",
            "agreements": {
                "use": true,
                "teenager": true,
                "privacy": true
            }
        }

    var daiosResponse = new DaiosResponse();
    if (req.body.email != null) {

        let country = req.body.country;
        let coins = req.body.coins;
        let agreements = req.body.agreements;
        let userTag = req.body.email;

        controllerUsers.getByUserTagAndEmail(userTag, req.body.email)
            .then(result => {

                if (result != null) {
                    let error = "userTag 혹은 userEmail에 이미 사용중입니다."
                    daiosResponse.code = 403;
                    daiosResponse.message = error;
                    res.status(403).send(daiosResponse.create())
                } else {
                    controllerCoins.createByCoin(coins)
                        .then(result => {
                            if (result._id != null) {
                                let coinId = result._id;
                                console.log('coinId=>', coinId)
                                controllerAgreements.createByAgreement(agreements)
                                    .then(result => {
                                        if (result._id != null) {
                                            let agreementId = result._id;
                                            controllerUsers.createWithIds(req, coinId, agreementId)
                                                .then(result => {

                                                    req.session.userTag = result.userTag;
                                                    req.session.userId = result._id;
                                                    req.session.active = false;
                                                    req.session.country = result.country;

                                                    daiosResponse.code = 200;
                                                    daiosResponse.data = result;
                                                    res.status(200).send(daiosResponse.create())
                                                })
                                        }
                                    }).catch((err) => {
                                    console.error('err=>', err)
                                    daiosResponse.code = 500;
                                    daiosResponse.message = err;
                                    res.status(500).send(daiosResponse.create())
                                })
                            }
                        })
                }

            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
})
// router.post('/', function (req, res, next) {
//
//     var daiosResponse = new DaiosResponse();
//     if (req.body.email != null) {
//         controllerUsers.create(req)
//             .then(result => {
//                 daiosResponse.code = 200;
//                 daiosResponse.data = result;
//                 res.status(200).send(daiosResponse.create())
//             }).catch((err) => {
//             console.error('err=>', err)
//             daiosResponse.code = 500;
//             daiosResponse.message = err;
//             res.status(500).send(daiosResponse.create())
//         })
//     }
// })

// router.put('/:userId', function (req, res, next) {
//
//     var daiosResponse = new DaiosResponse();
//     if (req.params.userId != null) {
//
//         if (req.body.ethers != null
//             || req.body.ethers.address != undefined) {
//
//             console.log('req.body1=>', req.body)
//             let ethers = req.body.ethers;
//             let controllerEthers = require('../controllers/ethers');
//             let controllerEtherHistorys = require('../controllers/etherHistorys');
//
//             //TODO: test code address
//             // ethers['token_address_contract'] = '0x04ea59838c45b7cff1ec4837984f480418d674af';
//
//             controllerEthers.createByEther(ethers)
//                 .then(result => {
//                     if (result._id != null) {
//                         let etherId = result._id;
//                         let init_coin = result.total_coin;
//
//                         controllerUsers.updateWithEther(req, etherId)
//                             .then(result => {
//                                 ethers['etherId'] = etherId;
//                                 ethers['coin'] = init_coin;
//
//                                 controllerEtherHistorys.createEtherHistoryByEtherId(etherId, ethers)
//                                     .then(result => {
//                                         let historyId = result._id;
//
//                                         controllerEthers.updateWithHistory(etherId, historyId)
//                                             .then(result => {
//                                                 controllerEthers.getEther(etherId)
//                                                     .then(result => {
//
//                                                         console.log('getEther result=>', result);
//                                                         //TODO: test json data
//                                                         // let historyCoin = req.body.total_coin;
//                                                         let historyCount = result.historys.length;
//                                                         var jsonData = {};
//                                                         jsonData['etherId'] = etherId;
//                                                         jsonData['historyId'] = historyId;
//                                                         jsonData['coin'] = 0;
//                                                         jsonData['address'] = result.address;
//                                                         jsonData['token_address_contract'] = result.token_address_contract;
//                                                         jsonData['address_count'] = historyCount;
//                                                         jsonData['total_coin'] = result.total_coin;
//                                                         jsonData['total_daios'] = result.total_daios;
//                                                         jsonData['historyCount'] = historyCount;
//
//                                                         mqtt.publishEtherScanSchduler(jsonData)
//                                                         mqtt.subscribeEtherScanSchduler();
//
//                                                         daiosResponse.code = 200;
//                                                         daiosResponse.data = result;
//                                                         res.status(200).send(daiosResponse.create())
//                                                     });
//                                             })
//                                     });
//                             }).catch((err) => {
//                             console.error('err=>', err)
//                             daiosResponse.code = 500;
//                             daiosResponse.message = err;
//                             res.status(500).send(daiosResponse.create())
//                         });
//                     }
//                 })
//
//         } else {
//             controllerUsers.update(req)
//                 .then(result => {
//                     daiosResponse.code = 200;
//                     daiosResponse.data = result;
//                     res.status(200).send(daiosResponse.create())
//                 }).catch((err) => {
//                 console.error('err=>', err)
//                 daiosResponse.code = 500;
//                 daiosResponse.message = err;
//                 res.status(500).send(daiosResponse.create())
//             })
//         }
//
//     }
// })

router.put('/:userId', function (req, res, next) {

    let sampleJson =
        {
            "first_name": "kyewon",
            "last_name": "seo",
            "country": "kr",
            "active": true,
            "phone": "010-123-4567",
            "agreements": {
                "use": true,
                "teenager": true,
                "privacy": true
            },
            "coins": {
                "dai_address": "asdf2",
                "btc_address": "qewr2",
                "ether_address": "zxcv"
            }
        }

    var daiosResponse = new DaiosResponse();
    if (req.params.userId != null) {

        let userId = req.params.userId;
        let country = req.body.country;
        let email = req.body.email;
        let coins = req.body.coins;
        let agreements = req.body.agreements;


        if (coins == undefined) coins = {}
        if (agreements == undefined) agreements = {}

        controllerUsers.update(req)
            .then((result) => {

                let resData = result;
                let coinId = result.coinId;
                let agreementId = result.agreementId;

                controllerCoins.updateById(coinId, coins)
                    .then(result => {
                        console.log('coinId=>', coinId)
                        controllerAgreements.updateById(agreementId, agreements)
                            .then(result => {
                                resData['address'] = result.address;
                                console.log('resData=>', resData);

                                if (country != undefined) req.session.country = country;
                                if (email != undefined) req.session.active = false;


                                daiosResponse.code = 200;
                                daiosResponse.data = resData;
                                res.status(200).send(daiosResponse.create())
                            })
                    })
            })
        // controllerCoins.createByCoin(coins)
        //     .then(result => {
        //         if (result._id != null) {
        //             let coinId = result._id;
        //             console.log('coinId=>', coinId)
        //             controllerAgreements.createByAgreement(agreements)
        //                 .then(result => {
        //                     if (result._id != null) {
        //                         let agreementId = result._id;
        //                         controllerUsers.updateWithIds(req.body, userId, coinId, agreementId)
        //                             .then(result => {
        //
        //                                 req.session.userId = result._id;
        //                                 if (country != undefined) req.session.country = country;
        //                                 if (email != undefined) req.session.active = false;
        //
        //                                 daiosResponse.code = 200;
        //                                 daiosResponse.data = result;
        //                                 res.status(200).send(daiosResponse.create())
        //                             })
        //                     }
        //                 }).catch((err) => {
        //                 console.error('err=>', err)
        //                 daiosResponse.code = 500;
        //                 daiosResponse.message = err;
        //                 res.status(500).send(daiosResponse.create())
        //             })
        //         }
        //     })
            .catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
})

router.delete('/:userId', function (req, res, next) {

    var daiosResponse = new DaiosResponse();
    if (req.params.userId != null) {
        controllerUsers.remove(req)
            .then(result => {
                daiosResponse.code = 200;
                daiosResponse.data = result;
                res.status(200).send(daiosResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            daiosResponse.code = 500;
            daiosResponse.message = err;
            res.status(500).send(daiosResponse.create())
        })
    }
})

router.get("/btcAddress/:address", function (req, res, next) {
    let address = req.params.address;
    var daiosResponse = new DaiosResponse();
    let result;

    if (Address.isValid(address, bitcore.Networks.testnet)) {
        console.log("Address isValid : True");
        result = true;

    } else {
        console.log("Address isValid : False");
        result = false;
    }

    daiosResponse.code = 200;
    daiosResponse.data = result;
    res.status(200).send(daiosResponse.create())
})

module.exports = router;
