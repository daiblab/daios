App = {
  web3Provider: null,
  contracts: {},

  init: function() {
    return App.initWeb3();
    
  },

  initWeb3: function() {
    
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      
    } else {
      App.web3Provider = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/0c938ce8db2547fdbb12f73b5d500aca0c938ce8db2547fdbb12f73b5d500aca"));
     
    }
      return App.initContract();

     
  }, 

  initContract: function() {
    $.ajaxSetup({async: false});  
    $.getJSON('TokenCrowdsale.json', function(data) {      
      var TokenCrowdsaleArtifact = data;
      App.contracts.TokenCrowdsale = TruffleContract(TokenCrowdsaleArtifact);
      App.contracts.TokenCrowdsale.setProvider(App.web3Provider);
      return App.getRaisedFunds(), App.getRaisedToken(), App.getGoalFunds(), App.getEndTime(), App.isFinalized(), App.getTokenPrice100(), App.isGoalReached(), App.getEthRefundValue(), App.gethasClosed(), App.isLockup();
    });
    $.getJSON('MintableToken.json', function(data) {
      var MintableTokenArtifact = data;
      App.contracts.MintableToken = TruffleContract(MintableTokenArtifact);
      App.contracts.MintableToken.setProvider(App.web3Provider);
      return App.getBalance(), App.getTokenContractAddress();
    });

    $.getJSON('PausableToken.json', function(data) {
      var PausableTokenArtifact = data;
      App.contracts.PausableToken = TruffleContract(PausableTokenArtifact);
      App.contracts.PausableToken.setProvider(App.web3Provider);
    });

    return App.bindEvents();

  },

    //토큰 구매
  handleBuyTokens: function send() {
    var amount = parseInt($('#Balance').val());
    

    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      var crowdsaleContractAddress = crowdsale.address;
      crowdsale.rate().then(function(rate){
        var rate = rate;

        web3.eth.sendTransaction({
          from: web3.eth.coinbase,
          to: crowdsaleContractAddress,
          value: web3.toWei(amount/rate, 'ether'),
          gas : 150000

        },function(error, result) {
          if (!error) {
            console.log('from : ' + web3.eth.coinbase);
            console.log('to : ' + crowdsaleContractAddress);
          } else {
            console.log("aaa"+error.message);
          }
          });
      });
    }).catch(function(err) {
      console.log("ERR");
    });
  },

    //토큰 구매량
  getBalance: function() {
    console.log('Getting balances...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.token();
      }).then(function(address){
        var token_contract_address = address;
        console.log('Token contract address: ' + token_contract_address);
        token_contract = App.contracts.MintableToken.at(token_contract_address);
        return token_contract.balanceOf(web3.eth.coinbase);
      }).then(function(balance) {
        console.log(balance);
        tokenBalance = balance; 
        $('#tokenBalance').text(tokenBalance.toString(10));
      }).catch(function(err) {
        console.log(err.message);
      });
  },

    //모인 이더
  getRaisedFunds: function(){
    console.log('Getting raised fund...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.weiRaised();
    }).then(function(result){
      EthRaised = Math.round(1000*result/1000000000000000000)/1000;
      $('#ETHRaised').text(EthRaised.toString(10));
      }).catch(function(err) {
          console.log(err.message);
        });
  },
    
    // 종료시간
  getEndTime: function(){
    console.log('Getting endtime...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.closingTime();
    }).then(function(result){
      endTime = new Date(result.c[0]*1000);      
      $('#EndTime').text(endTime);
      }).catch(function(err) {
          console.log(err.message);
        });
  },


    //컨트랙트 주소
  getTokenContractAddress: function(){
    console.log('Getting token contract address...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      crowdsaleAddress = crowdsale.address
      console.log('TokenCrowdsale Address: ' + crowdsaleAddress);
      return crowdsale.token()
    }).catch(function(err) {
      console.log(err.message);
      }).then(function(token_address){
      tokenContractAddress = token_address;
      console.log('Token Crowdsale Address: ' + tokenContractAddress);
      $('#TokenContractAddress').text(tokenContractAddress);
      });
  },

    //발행 토큰량
  handlGetotalSupply: function(event) {    
    event.preventDefault();
    console.log('Getting totalSupply ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.gettotalSupply();
    }).then(function(result){
          console.log(result);                      
          $('#lbltotalSupply').text(tokenBalance.toString(10));


          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  } 

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
