var Users = require('../libs/users');

function createUser (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        Users.findOneAndUpdate(
            {
                "email": data.email
            },
            {
                $set:data
            },
            {upsert: true, new: true},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('createUser done: ' + user._id)
                resolve(user)
            }
        )
    })
}

function getUserById (id) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {"_id": id},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByUserIdAndSessionId (userId, sessionId) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {
                "_id": userId ,
                "sessionId": sessionId
            },
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserByUserIdAndSessionId done: ' + user)
                let data = {
                    "userId": user._id,
                    "sessionId": user.sessionId
                }
                resolve(data)
            }
        )
    })
}

function getUserByEmail (email, body) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {"email": email},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function updateUserById(userId, body) {
    return new Promise((resolve, reject) => {
        Users.findOneAndUpdate(
            {"_id": userId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function deleteUserById (id) {
    return new Promise((resolve, reject) => {
        Users.findByIdAndRemove(
            id,
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByIds (userIds) {
    return new Promise((resolve, reject) => {
        Users.find(
            {"_id": {$in:userIds}},
            function(err, user) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }

                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByTag (userTag) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {"userTag": userTag},
            function(err, user) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                // console.log('getUserByTag done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByIdAndPassword (data) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {
                "userTag"        : data.userTag,
                "password"  : data.password
            },
            function(err, user) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                console.log('getUserByIdAndPassword done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByTagAndEmail (userTag, email) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {$or: [
                    {"userTag":userTag},
                    {"email": email},
                ]},
            function(err, user) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                console.log('getUserByTagAndEmail done: ' + user)
                resolve(user)
            }
        )
    })
}

exports.createUser = createUser;
exports.getUserById = getUserById;
exports.getUserByUserIdAndSessionId = getUserByUserIdAndSessionId;
exports.getUserByEmail = getUserByEmail;

exports.getUserByIds = getUserByIds;
exports.getUserByTag = getUserByTag;
exports.getUserByTagAndEmail = getUserByTagAndEmail;
exports.getUserByIdAndPassword = getUserByIdAndPassword;

exports.updateUserById = updateUserById;
exports.deleteUserById = deleteUserById;
