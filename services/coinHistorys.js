var CoinHistorys = require('../libs/coinHistorys');
var CoinEtherHistorys = require('../libs/coinEtherHistorys');
var CoinDaiHistorys = require('../libs/coinDaiHistorys');
var CoinBtcHistorys = require('../libs/coinBtcHistorys');
var CoinEtherWithdrawHistory = require('../libs/coinEtherWithdrawHistorys');
var CoinDaiWithdrawHistory = require('../libs/coinDaiWithdrawHistorys');
var CoinBtcWithdrawHistory = require('../libs/coinBtcWithdrawHistorys');

function createCoinHistory (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinHistorys = new CoinHistorys(data)
        coinHistorys.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinHistory done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinEtherHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinEtherHistorys = new CoinEtherHistorys(data)
        coinEtherHistorys.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinEtherHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinDaiHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinDaiHistorys = new CoinDaiHistorys(data)
        coinDaiHistorys.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinDaiHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinEtherWithdrawHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinEtherWithdrawHistory = new CoinEtherWithdrawHistory(data)
        coinEtherWithdrawHistory.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinEtherWithdrawHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinDaiWithdrawHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinDaiWithdrawHistory = new CoinDaiWithdrawHistory(data)
        coinDaiWithdrawHistory.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinEtherWithdrawHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinBtcWithdrawHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinBtcWithdrawHistory = new CoinBtcWithdrawHistory(data)
        coinBtcWithdrawHistory.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinBtcWithdrawHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinBtcHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinBtcHistorys = new CoinBtcHistorys(data)
        coinBtcHistorys.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinBtcHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function getCoinHistoryById (id) {
    return new Promise((resolve, reject) => {
        CoinHistorys.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinHistoryById done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinEtherHistorys (ids) {
    return new Promise((resolve, reject) => {
        CoinEtherHistorys.find(
            {'_id': { $in: ids}},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                for (var i = 0; i < coinHistory.length; i ++) {
                    coinHistory[i]._doc.type = "ether"
                }
                // console.log('getCoinEtherHistoryById done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinDaiHistorys (ids) {
    return new Promise((resolve, reject) => {
        CoinDaiHistorys.find(
            {'_id': { $in: ids}},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                for (var i = 0; i < coinHistory.length; i ++) {
                    coinHistory[i]._doc.type = "dai"
                }
                // console.log('getCoinDaiHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinBtcHistorys (ids) {
    return new Promise((resolve, reject) => {
        CoinBtcHistorys.find(
            {'_id': { $in: ids}},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                for (var i = 0; i < coinHistory.length; i ++) {
                    coinHistory[i]._doc.type = "btc"
                }
                // console.log('getCoinBtcHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinEtherWithdrawHistorysByIds (ids) {
    return new Promise((resolve, reject) => {
        CoinEtherWithdrawHistory.find(
            {'_id': { $in: ids}},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                for (var i = 0; i < coinHistory.length; i ++) {
                    coinHistory[i]._doc.type = "ether"
                }
                // console.log('getCoinEtherHistoryById done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinDaiWithdrawHistorysByIds (ids) {
    return new Promise((resolve, reject) => {
        CoinDaiWithdrawHistory.find(
            {'_id': { $in: ids}},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                for (var i = 0; i < coinHistory.length; i ++) {
                    coinHistory[i]._doc.type = "dai"
                }
                console.log('getCoinDaiHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinBtcWithdrawHistorysByIds (ids) {
    return new Promise((resolve, reject) => {
        CoinBtcWithdrawHistory.find(
            {'_id': { $in: ids}},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                for (var i = 0; i < coinHistory.length; i ++) {
                    coinHistory[i]._doc.type = "btc"
                }
                console.log('getCoinBtcHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinEtherHistoryById (id) {
    return new Promise((resolve, reject) => {
        CoinEtherHistorys.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinEtherHistoryById done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinDaiHistoryById (id) {
    return new Promise((resolve, reject) => {
        CoinDaiHistorys.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinDaiHistoryById done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinBtcHistoryById (id) {
    return new Promise((resolve, reject) => {
        CoinBtcHistorys.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinBtcHistoryById done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinEtherWithdrawHistorys (id) {
    return new Promise((resolve, reject) => {
        CoinEtherWithdrawHistory.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinEtherWithdrawHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinDaiWithdrawHistorys (id) {
    return new Promise((resolve, reject) => {
        CoinDaiWithdrawHistory.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinDaiWithdrawHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function getCoinBtcWithdrawHistorys (id) {
    return new Promise((resolve, reject) => {
        CoinBtcWithdrawHistory.findOne(
            {"_id": id},
            function(err, coinHistory) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinBtcWithdrawHistorys done: ' + coinHistory)
                resolve(coinHistory)
            }
        )
    })
}

function updateCoinHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinHistorys.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinEtherHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinEtherHistorys.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinDaiHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinDaiHistorys.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinBtcHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinBtcHistorys.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinEtherWithdrawHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinEtherWithdrawHistory.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinDaiWithdrawHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinDaiWithdrawHistory.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinBtcWithdrawHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinBtcWithdrawHistory.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

exports.createCoinHistory = createCoinHistory
exports.createCoinEtherHistorys = createCoinEtherHistorys
exports.createCoinDaiHistorys = createCoinDaiHistorys
exports.createCoinBtcHistorys = createCoinBtcHistorys
exports.createCoinEtherWithdrawHistorys = createCoinEtherWithdrawHistorys
exports.createCoinDaiWithdrawHistorys = createCoinDaiWithdrawHistorys
exports.createCoinBtcWithdrawHistorys = createCoinBtcWithdrawHistorys
exports.getCoinEtherHistorys = getCoinEtherHistorys
exports.getCoinDaiHistorys = getCoinDaiHistorys
exports.getCoinBtcHistorys = getCoinBtcHistorys
exports.getCoinEtherWithdrawHistorysByIds = getCoinEtherWithdrawHistorysByIds
exports.getCoinDaiWithdrawHistorysByIds = getCoinDaiWithdrawHistorysByIds
exports.getCoinBtcWithdrawHistorysByIds = getCoinBtcWithdrawHistorysByIds
exports.getCoinHistoryById = getCoinHistoryById
exports.getCoinEtherHistoryById = getCoinEtherHistoryById
exports.getCoinDaiHistoryById = getCoinDaiHistoryById
exports.getCoinBtcHistoryById = getCoinBtcHistoryById
exports.getCoinEtherWithdrawHistorys = getCoinEtherWithdrawHistorys
exports.getCoinDaiWithdrawHistorys = getCoinDaiWithdrawHistorys
exports.getCoinBtcWithdrawHistorys = getCoinBtcWithdrawHistorys
exports.updateCoinHistoryById = updateCoinHistoryById
exports.updateCoinEtherHistoryById = updateCoinEtherHistoryById
exports.updateCoinDaiHistoryById = updateCoinDaiHistoryById
exports.updateCoinBtcHistoryById = updateCoinBtcHistoryById
exports.updateCoinEtherWithdrawHistoryById = updateCoinEtherWithdrawHistoryById
exports.updateCoinDaiWithdrawHistoryById = updateCoinDaiWithdrawHistoryById
exports.updateCoinBtcWithdrawHistoryById = updateCoinBtcWithdrawHistoryById

