var Agreements = require('../libs/agreements');

function createAgreement (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        Agreements.findOneAndUpdate(
            {
                "use": data.use
            },
            {
                $set:data
            },
            {upsert: true, new: true},
            function(err, agreement) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                console.log('createAgreement done: ' + agreement._id)
                resolve(agreement)
            }
        )
    })
}

function updateAgreementById(id, body) {
    return new Promise((resolve, reject) => {
        Agreements.findOneAndUpdate(
            {"_id": id
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function getAgreementById (id) {
    return new Promise((resolve, reject) => {
        Agreements.findOne(
            {"_id": id},
            function(err, agreement) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                console.log('getEtherById done: ' + agreement)
                resolve(agreement)
            }
        )
    })
}

exports.createAgreement = createAgreement
exports.updateAgreementById = updateAgreementById
exports.getAgreementById = getAgreementById
