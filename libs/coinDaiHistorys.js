var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var coinDaiHistorysSchema = require('../models/coinDaiHistorys');

module.exports = mongoose.model('CoinDaiHistorys', coinDaiHistorysSchema);
