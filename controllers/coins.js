
var db = require('../utils/db');
var daiosCoins = require('../services/coins');
var util = require('../utils/util')

function get(req) {
    return new Promise((resolve, reject) => {

        var coinId = req.params.coinId;

        db.connectDB()
            .then(() => daiosCoins.getCoinById(coinId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function getByCoinId(coinId) {

    return new Promise((resolve, reject) => {
        db.connectDB()
            .then(() => daiosCoins.getCoinById(coinId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function create(req) {
    return new Promise((resolve, reject) => {

        if (req.body.total_dai == undefined) req['body']['total_dai'] = 0

        let data = req.body;

        db.connectDB()
            .then(() => daiosCoins.createCoin(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createByCoin(coin) {
    return new Promise((resolve, reject) => {

        var data = {};
        if (coin != undefined) data = coin;
        data['total_dai'] = 0;


        db.connectDB()
            .then(() => daiosCoins.createCoin(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createWithIds(req, coinId, agreementId, bankId) {
    return new Promise((resolve , reject) => {

        var data = req.body;
        delete data['coins'];
        delete data['agreements'];
        delete data['banks'];

        var password = crypto.createHash('sha256').update(req.body.password).digest('base64');
        data['password'] = password;
        data['coinId'] = coinId;
        data['agreementId'] = agreementId;
        data['bankId'] = bankId;
        data['regDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))

        console.log('createWithIds data=>', data);

        db.connectDB()
            .then(() => daiosCoins.createCoin(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function update(req) {
    return new Promise((resolve , reject) => {

        var coinId = req.params.coinId;

        var data = {};
        data['email'] = req.body.email;

        if (req.body.password != undefined) {
            var password = crypto.createHash('sha256').update(req.body.password).digest('base64');
            data['password'] = password;
        }

        db.connectDB()
            .then(() => daiosCoins.updateCoinById(coinId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateById(id, body) {
    return new Promise((resolve , reject) => {

        var data = body;

        db.connectDB()
            .then(() => daiosCoins.updateCoinById(id, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateWithHistory(coinType, coinId, historyId) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => {
                if (coinType == "ether") {
                    daiosCoins.updateCoinEtherHistoryId(coinId, historyId)
                } else if (coinType == "btc") {
                    daiosCoins.updateCoinBtcHistoryId(coinId, historyId)
                } else if (coinType == "dai") {
                    daiosCoins.updateCoinDaiHistoryId(coinId, historyId)
                } else {
                    daiosCoins.updateCoinHistoryId(coinId, historyId)
                }
            })
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateWithWithdrawHistory(coinType, coinId, historyId) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => {
                if (coinType == "ether") {
                    daiosCoins.updateCoinEtherWithdrawHistoryId(coinId, historyId)
                } else if (coinType == "dai") {
                    daiosCoins.updateCoinDaiWithdrawHistoryId(coinId, historyId)
                } else if (coinType == "btc") {
                    daiosCoins.updateCoinBtcWithdrawHistoryId(coinId, historyId)
                } else {
                    console.log('exception > updateWithWithdrawHistory')
                }
            })
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateTotalCoin(coinId, data) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => daiosCoins.updateCoinById(coinId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function remove(req) {
    return new Promise((resolve, reject) => {

        var coinId = req.params.coinId;

        db.connectDB()
            .then(() => daiosCoins.deleteCoinById(coinId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

exports.get = get;
exports.getByCoinId = getByCoinId;
exports.create = create;
exports.createByCoin = createByCoin;
exports.createWithIds = createWithIds;
exports.update = update;
exports.updateById = updateById;
exports.updateWithHistory = updateWithHistory;
exports.updateWithWithdrawHistory = updateWithWithdrawHistory;
exports.updateTotalCoin = updateTotalCoin;
exports.remove = remove;

