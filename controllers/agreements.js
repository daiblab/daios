var db = require('../utils/db');
var daiosAgreements = require('../services/agreements');

function get(req) {
    return new Promise((resolve, reject) => {

        var agreementId = req.params.agreementId;

        db.connectDB()
            .then(() => daiosAgreements.getAgreementById(agreementId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;

        db.connectDB()
            .then(() => daiosAgreements.createAgreement(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createByAgreement(agreement) {
    return new Promise((resolve, reject) => {

        var data = {};
        if (agreement != undefined) data = agreement;

        db.connectDB()
            .then(() => daiosAgreements.createAgreement(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function updateById(id, body) {
    return new Promise((resolve , reject) => {

        var data = body;

        db.connectDB()
            .then(() => daiosAgreements.updateAgreementById(id, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

exports.get = get;
exports.create = create;
exports.createByAgreement = createByAgreement;
exports.updateById = updateById;
