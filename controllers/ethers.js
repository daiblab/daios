var db = require('../utils/db');
var daiosEthers = require('../services/ethers');

function get(req) {
    return new Promise((resolve, reject) => {

        var etherId = req.params.etherId;
        db.connectDB()
            .then(() => daiosEthers.getEtherById(etherId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function getPrice(req) {
    return new Promise((resolve, reject) => {

        var etherId = req.params.etherId;
        //TODO: call daios block chain API
        var dac = 10
        var response = {'dac':dac}
        resolve(response);
    })
}

function getEther(etherId) {
    return new Promise((resolve, reject) => {
        db.connectDB()
            .then(() => daiosEthers.getEtherById(etherId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;
        db.connectDB()
            .then(() => daiosEthers.createEther(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createByEther(ether) {
    return new Promise((resolve, reject) => {

        var data = ether;
        db.connectDB()
            .then(() => daiosEthers.createEther(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function update(req) {
    return new Promise((resolve , reject) => {

        var etherId = req.params.etherId;
        var data = req.body;
        db.connectDB()
            .then(() => daiosEthers.updateEtherById(etherId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateWithHistory(etherId, historyId) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => daiosEthers.updateEtherHistoryId(etherId, historyId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateTotalCoin(etherId, data) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => daiosEthers.updateEtherById(etherId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

exports.get = get;
exports.getPrice = getPrice;
exports.getEther = getEther;
exports.create = create;
exports.createByEther = createByEther;
exports.update = update;
exports.updateWithHistory = updateWithHistory;
exports.updateTotalCoin = updateTotalCoin;
