
var db = require('../utils/db');
var daiosTest = require('../services/test');

function test(req) {
    return new Promise((resolve, reject) => {
        var login = req.params.userId;
        var data = {'login': login}
        db.connectDB()
            .then(() => daiosTest.createTest(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            })
    })

}

exports.test = test;
