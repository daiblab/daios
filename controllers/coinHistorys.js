var db = require('../utils/db');
var daiosCoinHistorys = require('../services/coinHistorys');
var util = require('../utils/util')

function getDepositHistorys(historyIds, coinType) {
    return new Promise((resolve, reject) => {

        if (historyIds.length > 0) {
            db.connectDB()
                .then(() => {

                    if (coinType == "ether") {
                        daiosCoinHistorys.getCoinEtherHistorys(historyIds)
                            .then((result) => {
                                console.log('result=>', result);
                                resolve(result)
                            })
                    } else if (coinType == "dai") {
                        daiosCoinHistorys.getCoinDaiHistorys(historyIds)
                            .then((result) => {
                                console.log('result=>', result);
                                resolve(result)
                            })
                    } else if (coinType == "btc") {
                        daiosCoinHistorys.getCoinBtcHistorys(historyIds)
                            .then((result) => {
                                console.log('result=>', result);
                                resolve(result)
                            })
                    } else {
                        console.log('not select ! getCoinHistoryById api')
                        reject(-1)
                    }
                }).catch((err) => {
                reject(err)
            })
        } else {
            resolve([])
        }
    })
}

function getWithdrawHistorys(historyIds, coinType) {
    return new Promise((resolve, reject) => {

        if (historyIds.length > 0) {
            db.connectDB()
                .then(() => {

                    if (coinType == "ether") {
                        daiosCoinHistorys.getCoinEtherWithdrawHistorysByIds(historyIds)
                            .then((result) => {
                                console.log('result=>', result);
                                resolve(result)
                            })
                    } else if (coinType == "dai") {
                        daiosCoinHistorys.getCoinDaiWithdrawHistorysByIds(historyIds)
                            .then((result) => {
                                console.log('result=>', result);
                                resolve(result)
                            })
                    } else if (coinType == "btc") {
                        daiosCoinHistorys.getCoinBtcWithdrawHistorysByIds(historyIds)
                            .then((result) => {
                                console.log('result=>', result);
                                resolve(result)
                            })
                    } else {
                        console.log('not select ! getCoinHistoryById api')
                        reject(-1)
                    }
                }).catch((err) => {
                reject(err)
            })
        } else {
            resolve([])
        }
    })
}

function getDeposit(req) {
    return new Promise((resolve, reject) => {

        var historyId = req.params.historyId;
        var coinType = req.params.coinType;

        db.connectDB()
            .then(() => {

                if (coinType == "ether") {
                    daiosCoinHistorys.getCoinEtherHistoryById(historyId)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        })
                } else if (coinType == "dai") {
                    daiosCoinHistorys.getCoinDaiHistoryById(historyId)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        })
                } else if (coinType == "btc") {
                    daiosCoinHistorys.getCoinBtcHistoryById(historyId)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        })
                } else {
                    console.log('not select ! getCoinHistoryById api')
                }
            }).catch((err) => {
            reject(err)
        })
    })

}

function getWithdraw(req) {
    return new Promise((resolve, reject) => {

        var historyId = req.params.historyId;
        var coinType = req.params.coinType;

        db.connectDB()
            .then(() => {

                if (coinType == "ether") {
                    daiosCoinHistorys.getCoinEtherWithdrawHistorys(historyId)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        })
                } else if (coinType == "dai") {
                    daiosCoinHistorys.getCoinDaiWithdrawHistorys(historyId)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        })
                } else if (coinType == "btc") {
                    daiosCoinHistorys.getCoinBtcWithdrawHistorys(historyId)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        })
                } else {
                    console.log('not select ! getCoinHistoryById api')
                    reject(-1)
                }
            }).catch((err) => {
            reject(err)
        })
    })

}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;

        db.connectDB()
            .then(() => daiosCoinHistorys.createCoinHistory(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createCoinHistoryByCoinId(coinId , req) {
    return new Promise((resolve, reject) => {

        var data = {};
        data['price'] = req.body.price;
        data['coinId'] = coinId;
        // data['type'] = req.params.coinType;
        data['regDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        data['completeDate'] = "";
        data['status'] = false;
        data['dai'] = req.body.dai;
        data['from_userId'] = req.body.from_userId;
        data['to_userId'] = req.body.to_userId;

        db.connectDB()
            .then(() => {
                if (req.params.coinType == "ether") {
                    daiosCoinHistorys.createCoinEtherHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (req.params.coinType == "btc") {
                    daiosCoinHistorys.createCoinBtcHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (req.params.coinType == "dai") {
                    daiosCoinHistorys.createCoinDaiHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else {
                    console.log('not create ! createCoinHistoryByCoinId api')
                    reject(-1)
                }
            })

    })

}

function createCoinWithdrawHistoryByCoinId(coinId , data) {
    return new Promise((resolve, reject) => {

        data['coinId'] = coinId;
        // data['type'] = req.params.coinType;
        data['regDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        data['completeDate'] = "";
        data['status'] = false;

        db.connectDB()
            .then(() => {
                if (data.coinType == "ether") {
                    daiosCoinHistorys.createCoinEtherWithdrawHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (data.coinType == "btc") {
                    daiosCoinHistorys.createCoinBtcWithdrawHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (data.coinType == "dai") {
                    daiosCoinHistorys.createCoinDaiWithdrawHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else {
                    daiosCoinHistorys.createCoinHistory(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                }
            })

    })

}

function update(req) {
    return new Promise((resolve , reject) => {

        var historyId = req.params.historyId;
        var data = req.body;

        db.connectDB()
            .then(() => daiosCoinHistorys.updateCoinHistoryById(historyId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateCoinHistoryStatusById(historyId, history) {
    return new Promise((resolve , reject) => {

        var data = {};
        data['completeDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        data['status'] = true;
        data['completedPrice'] = history.completedPrice;

        db.connectDB()
            .then(() => {

                if (history['type'] == "ether") {
                    daiosCoinHistorys.updateCoinEtherHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "btc") {
                    daiosCoinHistorys.updateCoinBtcHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "dai") {
                    daiosCoinHistorys.updateCoinDaiHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "ether_withdraw") {
                    daiosCoinHistorys.updateCoinEtherWithdrawHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "dai_withdraw") {
                    daiosCoinHistorys.updateCoinDaiWithdrawHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "btc_withdraw") {
                    daiosCoinHistorys.updateCoinBtcWithdrawHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                }
                else {
                    daiosCoinHistorys.updateCoinHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                }
            })

    })
}

function updateCoinEtherHistoryStatusById(historyId) {
    return new Promise((resolve , reject) => {

        var data = {};
        data['completeDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        data['status'] = true;
        db.connectDB()
            .then(() => daiosCoinHistorys.updateCoinEtherHistoryById(historyId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateCoinBtcHistoryStatusById(historyId) {
    return new Promise((resolve , reject) => {

        var data = {};
        data['completeDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        data['status'] = true;
        db.connectDB()
            .then(() => daiosCoinHistorys.updateCoinBtcHistoryById(historyId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

exports.getDepositHistorys = getDepositHistorys;
exports.getWithdrawHistorys = getWithdrawHistorys;
exports.getDeposit = getDeposit;
exports.getWithdraw = getWithdraw;
exports.create = create;
exports.createCoinHistoryByCoinId = createCoinHistoryByCoinId;
exports.createCoinWithdrawHistoryByCoinId = createCoinWithdrawHistoryByCoinId;
exports.update = update;
exports.updateCoinHistoryStatusById = updateCoinHistoryStatusById;
exports.updateCoinEtherHistoryStatusById = updateCoinEtherHistoryStatusById;
exports.updateCoinBtcHistoryStatusById = updateCoinBtcHistoryStatusById;
