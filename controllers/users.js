
var db = require('../utils/db');
var daiosUsers = require('../services/users');
var util = require('../utils/util')

function get(req) {
    return new Promise((resolve, reject) => {

        var userId = req.params.userId;
        db.connectDB()
            .then(() => daiosUsers.getUserById(userId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
                reject(err)
        })
    })

}

function getById(userId) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => daiosUsers.getUserById(userId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getByIds(userIds) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => daiosUsers.getUserByIds(userIds))
            .then((result) => {
                //console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getByUserTag(userTag) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => daiosUsers.getUserByTag(userTag))
            .then((result) => {
                // console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getByUserEmail(email) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => daiosUsers.getUserByEmail(email))
            .then((result) => {
                // console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getByUserTagAndEmail(userTag, email) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => daiosUsers.getUserByTagAndEmail(userTag, email))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getUser(data) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => daiosUsers.getUserByIdAndPassword(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function getUserIdAndSessionId(req) {
    return new Promise((resolve, reject) => {

        var userId = req.params.userId;
        var sessionId = req.params.sessionId;

        db.connectDB()
            .then(() => daiosUsers.getUserByUserIdAndSessionId(userId, sessionId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;
        data['userTag'] = data.email

        db.connectDB()
            .then(() => daiosUsers.createUser(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createWithIds(req, coinId, agreementId) {
    return new Promise((resolve , reject) => {

        var data = req.body;
        delete data['coins'];
        delete data['agreements'];

        if (data.email != undefined) data['userTag'] = data.email
        data['coinId'] = coinId;
        data['agreementId'] = agreementId;
        data['active'] = false;
        data['regDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))

        console.log('createWithIds data=>', data);

        db.connectDB()
            .then(() => daiosUsers.createUser(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateWithIds(body, userId, coinId, agreementId) {
    return new Promise((resolve , reject) => {

        var data = body;
        delete data['coins'];
        delete data['agreements'];

        if (data.email != undefined) data['userTag'] = data.email
        data['coinId'] = coinId;
        data['agreementId'] = agreementId;
        data['active'] = false;
        data['regDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))

        console.log('createWithIds data=>', data);

        db.connectDB()
            .then(() => daiosUsers.updateUserById(userId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function update(req) {
    return new Promise((resolve , reject) => {

        var userId = req.params.userId;
        var data = req.body;

        data['userTag'] = data.email;

        db.connectDB()
            .then(() => daiosUsers.updateUserById(userId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateEmailAuth(userId, data) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => daiosUsers.updateUserById(userId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

// function updateWithEther(req, etherId) {
//     return new Promise((resolve , reject) => {
//
//         var userId = req.params.userId;
//         var data = req.body;
//         delete data['ethers'];
//         // delete data['coins'];
//         // delete data['agreements'];
//         // delete data['points'];
//
//         // console.log('updateWithEther data=>', data);
//
//         db.connectDB()
//             .then(() => daiosUsers.updateUserById(userId, data))
//             .then((result) => {
//                 console.log('result=>' , result);
//                 resolve(result)
//             }).catch((err) => {
//             reject(err)
//         })
//     })
// }

function remove(req) {
    return new Promise((resolve, reject) => {

        var userId = req.params.userId;
        db.connectDB()
            .then(() => daiosUsers.deleteUserById(userId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

exports.get = get;
exports.getUserIdAndSessionId = getUserIdAndSessionId;
exports.getById = getById;
exports.getByIds = getByIds;
exports.getByUserTag = getByUserTag;
exports.getByUserEmail = getByUserEmail;
exports.getByUserTagAndEmail = getByUserTagAndEmail;
exports.getUser = getUser;
exports.create = create;
exports.createWithIds = createWithIds;
exports.update = update;
exports.updateWithIds = updateWithIds;
exports.updateEmailAuth = updateEmailAuth;
// exports.updateWithEther = updateWithEther;
exports.remove = remove;

